import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { createProficienciesList } from './createProficienciesList';
import { createLanguagesList } from './createLanguagesList';

const basicInfoSchema = Yup.object().shape({
    chosenName: Yup.string().notRequired(),
    chosenClass: Yup.string().required('Class is required'),
    chosenRace: Yup.string().required('Race is required'),
  });

const BasicInfo = ()  => {
    const [getName, setName] = useState("");
    const [classOptions, setClassOptions] = useState([]);
    const [raceOptions, setRaceOptions] = useState([]);
    const [getClass, setClass] = useState("");
    const [getRace, setRace] = useState("");
    const [getProficiencies, setProficiencies] = useState([]);
    const [getLanguages, setLanguages] = useState([]);

    useEffect( () => {
        axios
        .get('http://www.dnd5eapi.co/api/classes/')
        .then(res => setClassOptions(res.data.results));
        axios
        .get('http://www.dnd5eapi.co/api/races/')
        .then(res => setRaceOptions(res.data.results));
    }, []);

    const handleSubmit = (e) => {

        console.log(e);

        const chosenClass = e.chosenClass.name;
        const chosenRace = e.chosenRace.name;
        const chosenClassIndex = e.chosenClass.url;
        const chosenRaceIndex = e.chosenRace.url;

        setClass(chosenClass);
        setRace(chosenRace);
        getRaceLanguages(chosenRaceIndex);
        getClassProficiencies(chosenClassIndex);
    }

    function getRaceLanguages(raceIndex) {
        axios
        .get(`http://www.dnd5eapi.co${raceIndex}`)
        .then(res => {
            let lang= [];
            res.data.languages.forEach((object) => lang.push(object.name));
            setLanguages(lang);
        });
    };

    function getClassProficiencies(classIndex) {
        axios
        .get(`http://www.dnd5eapi.co${classIndex}`)
        .then(res => {
            let prof= [];
            res.data.proficiencies.forEach((object) => prof.push(object.name));
            setProficiencies(prof);
        });
    };

    const setCharachterName = e => {
        setName(e);
    };

    return(
        <>
            <h2>Choose name, race & class</h2>
            <Formik>
                <Field name="chosenName" render={({field}) =>
                    <span className='p-float-label' style={{ marginTop: '1rem' }}>
                        <InputText id='chosenName' {...field}  value={getName} onChange={e => {
                            setCharachterName(e.target.value)}} style={{ width: '100%' }}/>
                        <label htmlFor='chosenName'>Enter Name</label>
                    </span>
                }/>
            </Formik>
            <Formik
                initialValues={{
                  chosenClass: '',
                  chosenRace: ''
                }}
                validationSchema={basicInfoSchema}
                onSubmit={e => handleSubmit(e)}
                render={() => (
                    
                    <Form>
                <div>
                    <h2>
                        <Field name="chosenRace" render={({field}) =>
                        <Dropdown
                            {...field}
                            style={{ marginTop: '1rem' },{ width: '100%' }}
                            optionLabel="name"
                            options={raceOptions}
                            placeholder="Select D&D Race"/>}/>
                        <ErrorMessage name='chosenRace'/>
                    </h2>
                </div>
                <div>
                    <h2>
                        <Field name="chosenClass" render={({field}) =>
                        <Dropdown
                            {...field}
                            style={{ marginTop: '1rem' },{ width: '100%' }}
                            optionLabel="name"
                            options={classOptions}
                            placeholder="Select D&D Class"/>}/>
                        <ErrorMessage name='chosenClass'/>
                    </h2>
                </div>
                <div>
                    <h2>
                        <Button label="Select" className="p-button-raised" style={{ marginTop: '1rem' }}/>
                    </h2>
                </div>
                    </Form>
                )}
            />
            <div>
                <h3 className="NameRaceClass">{getName} {(getName && getRace && getClass) ? "the" : null} {getRace} {getClass}</h3>
            </div>
            <div>
                {createProficienciesList(getProficiencies)}
            </div>
            <div>
                {createLanguagesList(getLanguages)}
            </div>
        </>
    );
};

export default BasicInfo;
